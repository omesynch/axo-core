"use strict";
var gulp = require("gulp");
var pug = require("gulp-pug");
var sass = require("gulp-sass");
var minify = require("gulp-minify");
var browserSync = require("browser-sync").create();

gulp.task('serve', function() {

    browserSync.init({
        server: "./live/"
    });

    gulp.watch("./dev/sass/*.sass", ['sass']);
    gulp.watch("./dev/pages/*.pug", ['html']);
    gulp.watch("./dev/scripts/*.js", ['scripts']);
});

gulp.task("html", function(){
    gulp.src("./dev/pages/*.pug")
    .pipe(pug({pretty : true}))
    .pipe(gulp.dest("./live/"))
    .pipe(browserSync.stream());
});

gulp.task("sass", function(){
    gulp.src("./dev/sass/style.sass")
    .pipe(sass())
    .pipe(gulp.dest("./live/css/"))
    .pipe(browserSync.stream());
});

gulp.task("scripts", ["html"], function(){
    gulp.src("./dev/scripts/*.js")
    .pipe(minify())
    .pipe(gulp.dest("./live/scripts"))
    .pipe(browserSync.stream());
})

gulp.task("default",["html", "sass", "scripts"], function(){

})